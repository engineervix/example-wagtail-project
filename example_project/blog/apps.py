from django.apps import AppConfig


class BlogConfig(AppConfig):
    name = "example_project.blog"
