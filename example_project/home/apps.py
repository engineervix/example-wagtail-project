from django.apps import AppConfig


class HomeConfig(AppConfig):
    name = "example_project.home"
