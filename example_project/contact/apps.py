from django.apps import AppConfig


class ContactConfig(AppConfig):
    name = "example_project.contact"
